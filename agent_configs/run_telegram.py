import argparse
import json
import logging
import os

import yaml

from deeppavlov_agent.core.agent import Agent
from deeppavlov_agent.core.connectors import EventSetOutputConnector, PredefinedTextConnector
from deeppavlov_agent.core.db import DataBase
from deeppavlov_agent.core.log import LocalResponseLogger
from deeppavlov_agent.core.pipeline import Pipeline
from deeppavlov_agent.core.service import Service
from deeppavlov_agent.core.state_manager import StateManager
from deeppavlov_agent.core.workflow_manager import WorkflowManager
from deeppavlov_agent.parse_config import PipelineConfigParser
from telegram_client import run_tg

yaml.warnings({"YAMLLoadWarning": False})

service_logger = logging.getLogger("service_logger")

parser = argparse.ArgumentParser()
parser.add_argument(
    "-pl", "--pipeline_config", help="service name for service run mode", type=str, default="pipeline_conf.json"
)
parser.add_argument("-db", "--db_config", help="service name for service run mode", type=str, default="db_conf.json")
parser.add_argument("-px", "--proxy", help="proxy for telegram client", type=str, default="")
parser.add_argument("-t", "--token", help="token for telegram client", type=str)
parser.add_argument("-d", "--debug", help="run in debug mode", action="store_true")
parser.add_argument("-rl", "--response_logger", help="run agent with services response logging", action="store_true")
args = parser.parse_args()


def main():
    with open(args.db_config, "r") as db_config:
        if args.db_config.endswith(".json"):
            db_data = json.load(db_config)
        elif args.db_config.endswith(".yml"):
            db_data = yaml.load(db_config)
        else:
            raise ValueError("unknown format for db_config")

    if db_data.pop("env", False):
        for k, v in db_data.items():
            db_data[k] = os.getenv(v)

    db = DataBase(**db_data)

    sm = StateManager(db.get_db())

    with open(args.pipeline_config, "r") as pipeline_config:
        if args.pipeline_config.endswith(".json"):
            pipeline_data = json.load(pipeline_config)
        elif args.pipeline_config.endswith(".yml"):
            pipeline_data = yaml.load(pipeline_config)
        else:
            raise ValueError("unknown format for pipeline_config")
    pipeline_config = PipelineConfigParser(sm, pipeline_data)

    input_srv = Service("input", None, sm.add_human_utterance, 1, ["input"])
    responder_srv = Service("responder", EventSetOutputConnector("responder").send, sm.save_dialog, 1, ["responder"])

    last_chance_srv = pipeline_config.last_chance_service or Service(
        "last_chance",
        PredefinedTextConnector("Извините, что-то пошло не так").send,
        sm.add_bot_utterance_last_chance,
        1,
        ["last_chance"],
    )
    timeout_srv = pipeline_config.timeout_service or Service(
        "timeout",
        PredefinedTextConnector(
            "Извините, мне нужно больше времени, чтобы обдумать это. Давай поговорим о другом"
        ).send,
        sm.add_bot_utterance_last_chance,
        1,
        ["timeout"],
    )

    pipeline = Pipeline(pipeline_config.services, input_srv, responder_srv, last_chance_srv, timeout_srv)

    response_logger = LocalResponseLogger(args.response_logger)
    agent = Agent(pipeline, sm, WorkflowManager(), response_logger=response_logger)
    if pipeline_config.gateway:
        pipeline_config.gateway.on_channel_callback = agent.register_msg
        pipeline_config.gateway.on_service_callback = agent.process
    try:
        run_tg(args.token, args.proxy, agent)
    except Exception as e:
        raise e
    finally:
        logging.shutdown()


if __name__ == "__main__":
    main()
