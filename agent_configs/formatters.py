import copy
import json
import random
from typing import Dict, List


def rule_based_selector_formatter_in(dialog: Dict):
    dialog_len = len(dialog["utterances"])
    basic_intent = dialog["utterances"][-1]["annotations"]["basic_intent_classifier"]
    return [{"dialog_len": [dialog_len], "basic_intent": [basic_intent]}]


def get_corrected_text(utt):
    return utt.get("annotations", {}).get("spell_checker", {}).get("text", utt["text"])


def base_last_utterances_formatter_in(dialog: Dict, model_args_names=("x",)):
    return [{model_args_names[0]: [get_corrected_text(dialog["utterances"][-1])]}]


def base_utterances_formatter_in(dialog: Dict, model_args_names=("x",)):
    return [{model_args_names[0]: [[get_corrected_text(utt) for utt in dialog["utterances"]]]}]


def all_hypotheses_formatter_in(dialog: Dict):
    return [{"hypotheses": dialog["utterances"][-1]["hypotheses"]}]


def odqa_formatter_in(dialog: Dict, model_args_names=("question_raw",)):
    return [{model_args_names[0]: [get_corrected_text(dialog["utterances"][-1])]}]


def rule_based_selector_formatter_out(payload: List):
    if payload:
        print(f"rule_based_selector chooses {payload}")
        return payload
    else:
        raise ValueError("Empty payload provided")


def base_annotator_formatter_out(payload: List):
    if payload:
        return payload
    else:
        raise ValueError("Empty payload provided")


def spell_checker_formatter_out(payload: List):
    if payload:
        return payload
    else:
        return {}


def basic_intent_classifier_formatter_out(payload: List):
    if payload:
        class_name = payload[0]
        return [class_name if class_name in ["chit-chat"] else "odqa"]
    else:
        raise ValueError("Empty payload provided")


def ranking_chitchat_formatter_in(dialog: Dict) -> List:
    return [
        {
            "json_dump_of_data": [
                json.dumps(
                    {
                        "last_utterances": get_corrected_text(dialog["utterances"][-1]),
                        "utterances_histories": [get_corrected_text(utt) for utt in dialog["utterances"]],
                    }
                )
            ]
        }
    ]


def add_confidence_formatter_out(payload: List, confidence=0.5):
    if payload:
        return [{"text": payload[0], "confidence": 0.5}]
    else:
        raise ValueError("Empty payload provided")


def confidence_formatter_out(payload):
    if isinstance(payload[0], (list, tuple)):
        return [{"text": t, "confidence": c} for t, c in zip(*payload)]
    else:
        return [{"text": payload[0], "confidence": payload[1]}]


noanswers = [
    "Извините, я не знаю ответ на это",
    "Я хочу ответить, но моих знаний пока недостаточно",
    "Жаль, что мне нечего сказать в ответ, но я учусь и когда-нибудь у меня будет подходящий ответ",
    "Мне нечего сказать на это",
    "Мне бы кто-нибудь помог ответить на это",
    "Я пока не готов дать ответ",
]


def add_confidence_with_noanswer_formatter_out(payload: List, confidence=0.5):
    next_payload = copy.deepcopy(payload)
    next_payload[0] = next_payload[0] if next_payload[0] else random.choice(noanswers)
    return add_confidence_formatter_out(payload=next_payload, confidence=confidence)


def confidence_with_noanswer_formatter_out(payload: List):
    next_payload = copy.deepcopy(payload)
    next_payload[0] = next_payload[0] if next_payload[0] else random.choice(noanswers)
    return confidence_formatter_out(payload=next_payload)


doubt_texts = [
    "Я могу ошибаться, но мне кажется ответом будет",
    "Я не уверен, думаю можно ответить, что",
    "Думаю можно сказать, что",
]


def odqa_formatter_out(payload: List, confidence=0.5):
    responses = add_confidence_with_noanswer_formatter_out(payload=payload, confidence=confidence)
    responses = [
        (
            {"text": f"{random.choice(doubt_texts)}: {response['text']}", "confidence": response["confidence"]}
            if response["text"] not in noanswers
            else {"text": response["text"], "confidence": response["confidence"]}
        )
        for response in responses
    ]
    return responses


def response_selector_formatter_in(dialog: Dict):
    return [{"dialogs": [dialog]}]


def response_selector_formatter_out(payload: List):
    if len(payload) == 3:
        return {"skill_name": payload[0], "text": payload[1], "confidence": payload[2]}
    elif len(payload) == 5:
        return {
            "skill_name": payload[0],
            "text": payload[1],
            "confidence": payload[2],
            "human_attributes": payload[3],
            "bot_attributes": payload[4],
        }
