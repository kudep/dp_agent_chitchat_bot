## Workflow

We use [gitflow](https://newsignature.com/articles/git-branching-strategies-which-one-should-i-pick/) , you are welcome to `dev`

## Requirements

- python3

## MRs

All commits are checked for linting by CI. Before you send Merge Request, please, run `bash tools/format.sh` to fix style.
All requirements for dev tools installing by `pip install -r dev_requirements.txt`

## Service Ports

- odqa:2080
- rule_based_response_selector:2081
- basic_intent_classifier:2082
- aiml_chitchat:2083
- spell_checker:2084
- ranking_chitchat_2stage:2085
- rule_based_selector:2086
- greeting_skill:2087
- agent:4242
- mongo:27017
- insult_defense_skill:2088
- tg_proxy:2089

## Assemble full documentation

```bash
pip install grip
head -n 10000 README.md services/*/*/README.md CONTRIBUTING.md > venv/README.md
grip venv/README.md 8080 
```

