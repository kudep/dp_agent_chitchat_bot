#!/bin/sh

cd "$(dirname "$0")"
cd ..

set -e
set -x

. ./dev.env

black  --check --line-length=$LINE_LENGTH $LINTED_PATH

# isort --multi-line=$MULTI_LINE_IMPORTS --trailing-comma \
#     --force-grid-wrap=$FORCE_GRID_WRAP --combine-as \
#     --line-width $LINE_LENGTH --recursive --check-only \
#     --thirdparty $THIRDPARTY $LINTED_PATH