#!/bin/sh

cd "$(dirname "$0")"
cd ..

set -e
set -x


. ./dev.env

# Sort imports one per line, so autoflake can remove unused imports
isort --recursive  --force-single-line-imports \
    --thirdparty $THIRDPARTY --apply $LINTED_PATH

autoflake --remove-all-unused-imports --recursive \
    --remove-unused-variables --in-place $LINTED_PATH \
    --exclude=__init__.py

black --line-length=$LINE_LENGTH $LINTED_PATH

isort --multi-line=$MULTI_LINE_IMPORTS --trailing-comma \
    --force-grid-wrap=$FORCE_GRID_WRAP --combine-as \
    --line-width $LINE_LENGTH --recursive --thirdparty $THIRDPARTY \
    --apply $LINTED_PATH