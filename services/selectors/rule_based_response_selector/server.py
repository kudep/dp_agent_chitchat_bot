# Copyright 2017 Neural Networks and Deep Learning lab, MIPT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import difflib
import logging
import os
import re
import time
import traceback

import numpy as np
from flask import Flask, jsonify, request

import dirty_hacks

SERVICE_NAME = os.getenv("SERVICE_NAME", "unknow_skill")
SERVICE_PORT = int(os.getenv("SERVICE_PORT", 3000))

SPEC_TAG_MAP = {
    "BOT_NAME_NOM": os.getenv("BOT_NAME_NOM"),  # Именительный
    "BOT_NAME_GEN": os.getenv("BOT_NAME_GEN"),  # Родительный
    "BOT_NAME_DAT": os.getenv("BOT_NAME_DAT"),  # Дательный
    "BOT_NAME_ACC": os.getenv("BOT_NAME_ACC"),  # Винительный
    "BOT_NAME_INST": os.getenv("BOT_NAME_INST"),  # Творительный
    "BOT_NAME_PRE": os.getenv("BOT_NAME_PRE"),  # Предложный
    "USER_NAME_NOM": os.getenv("USER_NAME_NOM"),  # Именительный
    "USER_NAME_GEN": os.getenv("USER_NAME_GEN"),  # Родительный
    "USER_NAME_DAT": os.getenv("USER_NAME_DAT"),  # Дательный
    "USER_NAME_ACC": os.getenv("USER_NAME_ACC"),  # Винительный
    "USER_NAME_INST": os.getenv("USER_NAME_INST"),  # Творительный
    "USER_NAME_PRE": os.getenv("BOT_NAME_PRE"),  # Предложный
}

FORBIDDEN_SPEC_TAGS = [k for k, v in SPEC_TAG_MAP.items() if v is None]
forbidden_spec_tag_pat = re.compile("(" + ")|(".join(FORBIDDEN_SPEC_TAGS) + ")")
SPEC_TAG_MAP = {k: v for k, v in SPEC_TAG_MAP.items() if v is not None}


logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)

app = Flask(__name__)

MIN_CONFIDENCE = 0.01
MAX_SIMILARITY_RATIO = 0.7
MAX_CHECKING_HISTORY_LENGTH = 20
ERROR_CANDIDATE = {
    "skill_name": "error",
    "text": "У меня нет ответа на это",
    "clean_text": "у меня нет ответа на это",
    "confidence": MIN_CONFIDENCE,
}

NULL_HYPOTHESES = [
    "Уууу",
    "Ничоси, такого я не ожидала",
    "Уффф",
    "Эээ, ну...",
    "А можно я промолчу?",
    "Мне точно сейчас нужно отвечать что-нибудь?",
    "Можно воздержаться от ответа?",
    "От нас ботов все время что-то требуют, что-то спрашивают, а мы иногда хотим просто послушать Вас. "
    "Расскажете что-нибудь?",
    "Часто думаю, правда ли, что незнание чего-либо - это плохо? Нельзя же все знать, тем более мне.",
    "О чем бы Вы еще хотели поговорить?",
    "А может поговорим о нас?",
    "Как вы относитесь к тому, что Вам на вопрос вопросом отвечают? Просто я иногда так отвечаю)",
]

spaces_pat = re.compile(r"\s+")
special_symb_pat = re.compile(r"[^a-zа-я0-9 ]")
newline_pat = re.compile(r".{2,}\n.{2,}")
number_pat = re.compile(r"[0-9]{1,}")


def clean_text(text):
    return special_symb_pat.sub("", spaces_pat.sub(" ", text.lower().replace("\n", " "))).strip()


NULL_HYPOTHESES = [
    {"skill_name": "null_hypothesis", "text": hyp, "clean_text": clean_text(hyp), "confidence": MIN_CONFIDENCE,}
    for hyp in NULL_HYPOTHESES
]


def replace_spec_tag(text):
    # TODO: rm dirty_hacks
    text = dirty_hacks.fix_name_bugs(text)
    for spec_tag, replacement in SPEC_TAG_MAP.items():
        text = text.replace(spec_tag, replacement)
    return text


def safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default


def check_seq_matching(uttr, used_uttrs, max_similarity_ratio):
    return bool(
        [
            None
            for used_uttr in used_uttrs
            if difflib.SequenceMatcher(None, uttr.split(), used_uttr.split()).ratio() > max_similarity_ratio
        ]
    )


def filter_candidates(response_candidates, past_responses):
    # TODO: rm dirty_hacks
    response_candidates = dirty_hacks.fix_hello_bugs(response_candidates)
    # rm empty text
    response_candidates = [cand for cand in response_candidates if cand["clean_text"]]
    # rm forbidden spec tags
    response_candidates = [
        cand
        for cand in response_candidates
        if not (FORBIDDEN_SPEC_TAGS and forbidden_spec_tag_pat.search(cand["clean_text"]))
    ]
    # rm low confidence
    response_candidates = [cand for cand in response_candidates if MIN_CONFIDENCE <= cand["confidence"]]

    # choose trusty_responses
    trusty_response_candidates = [cand for cand in response_candidates if cand["confidence"] == 1.0]
    response_candidates = trusty_response_candidates if trusty_response_candidates else response_candidates

    # rm repetitions excluding aiml_chitchat
    response_candidates = [
        cand
        for cand in response_candidates
        if cand["skill_name"] in ["aiml_chitchat", "insult_defense_skill"]
        or not check_seq_matching(
            cand["clean_text"], past_responses[-(MAX_CHECKING_HISTORY_LENGTH // 2) :], MAX_SIMILARITY_RATIO
        )
    ]

    # rm cands with newline
    response_candidates = [
        cand
        for cand in response_candidates
        if not (newline_pat.search(cand["text"]) and cand["skill_name"] in ["ranking_chitchat_2stage"])
    ]

    # rm cands with numbers
    response_candidates = [
        cand
        for cand in response_candidates
        if not (number_pat.search(cand["text"]) and cand["skill_name"] in ["ranking_chitchat_2stage"])
    ]
    return response_candidates


def sample_candidates(candidates, choice_num, replace=False):
    choice_num = min(choice_num, len(candidates))
    confidences = [cand["confidence"] for cand in candidates]
    sum_conf = sum(confidences, 0)
    sum_conf = sum_conf if sum_conf > 0 else 1
    choice_probs = [conf / sum_conf for conf in confidences]
    return np.random.choice(candidates, choice_num, replace=replace, p=choice_probs)


def get_corrected_text(utt):
    return utt.get("annotations", {}).get("spell_checker", {}).get("text", utt["text"])


def response_selector(dialog):
    response_candidates = []
    for hyp in dialog["utterances"][-1]["hypotheses"]:
        text = replace_spec_tag(hyp["text"])
        response_candidates.append(
            {
                "skill_name": hyp["skill_name"],
                "text": text,
                "clean_text": clean_text(text),
                "confidence": min(max(safe_cast(hyp["confidence"], float, 0), 0), 1),
            }
        )
    response_candidates.extend(NULL_HYPOTHESES)
    past_utterances = [get_corrected_text(uttr) for uttr in dialog["utterances"][::-1][::2][::-1]]
    past_responses = [uttr["text"] for uttr in dialog["utterances"][::-1][1::2][::-1]]

    # clean text
    past_utterances = [clean_text(uttr) for uttr in past_utterances]
    past_responses = [clean_text(uttr) for uttr in past_responses]
    # logger.info(f"response_candidates before filtering {response_candidates}")
    response_candidates = filter_candidates(response_candidates, past_responses)
    # logger.info(f"response_candidates after filtering {response_candidates}")
    # sampling of a candidate
    try:
        candidate = (
            sample_candidates(response_candidates, 1)[0] if response_candidates else np.random.choice(NULL_HYPOTHESES)
        )
    except Exception:
        logger.error(f"dialog: {dialog}")
        logger.error(traceback.format_exc())
        candidate = ERROR_CANDIDATE

    return (
        candidate["skill_name"],
        candidate["text"],
        candidate["confidence"],
    )


@app.route("/model", methods=["POST"])
def respond():
    """A handler of requests.
    To use:
    curl -X POST "http://localhost:${PORT}/model" \
    -H "accept: application/json"  -H "Content-Type: application/json" \
    -d "{ \"args\": [ \"data\" ]}"
    """
    st_time = time.time()
    dialogs = request.json["dialogs"]
    response = [response_selector(dialog) for dialog in dialogs]

    total_time = time.time() - st_time
    logger.info(f"{SERVICE_NAME} exec time: {total_time:.3f}s")
    # logger.info(f"output {response}")
    return jsonify(response)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=SERVICE_PORT)
