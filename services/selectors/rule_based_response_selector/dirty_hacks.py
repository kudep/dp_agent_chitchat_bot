# Copyright 2017 Neural Networks and Deep Learning lab, MIPT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

name_nom_pat = re.compile("([Аа][Лл][Ии][Сс][Аа])|(Игорь)|(Татьяна)")
name_gen_pat = re.compile("([Аа][Лл][Ии][Сс][Ыы])")


def fix_name_bugs(text):
    text = name_nom_pat.sub("BOT_NAME_NOM", text)
    text = name_gen_pat.sub("BOT_NAME_GEN", text)
    return text


def fix_hello_bugs(cands):
    return [
        cand for cand in cands if not (cand["skill_name"] in ["ranking_chitchat_2stage"] and "привет" in cand["text"])
    ]
