# Copyright 2017 Neural Networks and Deep Learning lab, MIPT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pathlib import Path

from deeppavlov.core.common.file import read_json, save_json
from deeppavlov.utils import settings

settings_path = Path(settings.__path__[0]) / "server_config.json"

settings = read_json(settings_path)
settings["model_defaults"]["Chitchat"] = {
    "host": "",
    "port": "",
    "model_endpoint": "/model",
    "model_args_names": ["utterances", "annotations", "u_histories", "dialogs"],
}
save_json(settings, settings_path)
