# Copyright 2017 Neural Networks and Deep Learning lab, MIPT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import time
import traceback

import requests
from flask import Flask, jsonify, request

SERVICE_NAME = os.getenv("SERVICE_NAME", "unknow_skill")
SERVICE_PORT = int(os.getenv("SERVICE_PORT", 3000))
REQUEST_TIMEOUT = float(os.getenv("REQUEST_TIMEOUT", "0.5"))  # seconds


logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)

app = Flask(__name__)


url = "https://speller.yandex.net/services/spellservice.json/checkText"


def ya_speller(text: str):
    corrected = text
    try:
        r = requests.post(
            url,
            data=f"text={text}".encode(),
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            timeout=REQUEST_TIMEOUT,
        )
        for c in reversed(r.json()):
            if c.get("s"):
                corrected = corrected[: c["pos"]] + c["s"][0] + corrected[c["pos"] + c["len"] :]
    except Exception:
        logger.warning(traceback.format_exc())
        return {}
    return {"text": corrected}


@app.route("/model", methods=["POST"])
def respond():
    """A handler of requests.
    To use:
    curl -X POST "http://localhost:${PORT}/model" \
    -H "accept: application/json"  -H "Content-Type: application/json" \
    -d "{ \"args\": [ \"data\" ]}"
    """
    st_time = time.time()
    logger.info(f"input data: {request.json}")
    responses = [ya_speller(uttr) for uttr in request.json.get("x", [])]
    total_time = time.time() - st_time
    logger.info(f"{SERVICE_NAME} exec time: {total_time:.3f}s")
    return jsonify(responses)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=SERVICE_PORT)
