# Copyright 2017 Neural Networks and Deep Learning lab, MIPT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import random
import time

from flask import Flask, jsonify, request

SERVICE_NAME = os.getenv("SERVICE_NAME", "unknow_skill")
SERVICE_PORT = int(os.getenv("SERVICE_PORT", 3000))


logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)

app = Flask(__name__)


greeting_begin_texts = [
    "Хорошо здоровается тот, кто здоровается первым.",
    "Я встретил Вас. Значит: «День добрый!»",
    "'Очень добрый день! А это там что? И это все мне ?!!' ©Маша и медведь",
    "'Я пришёл к тебе с приветом, топором и пистолетом.' ©Источник неизвестен",
    "Раньше когда люди здоровались, снимали шляпу, а сейчас при встрече вытаскивают наушники из ушей.",
]
greeting_body_text = (
    "Надеюсь, у Вас хорошее настроение, я чат-бот и готов пообщаться в свободной форме. "
    "Давайте поговорим про Вас, про меня, про мир, который нас окружает, про то, что мы любим и ненавидим, "
    "про то, что мы хотим и про то, что нас беспокоит. "
    "Мои ответы на вопросы о известных фактах могут быть не совсем верными или даже совсем неверными, "
    "если это выходит за рамки моих знаний. К моему сожалению, я сейчас не так много знаю. "
    "\nЯ с радостью постараюсь поддержать диалог на любую интересную для Вас тему."
)
greeting_end_texts = [
    "Как дела?",
    "О чем бы ты хотел поговорить с чат-ботом?",
    "Что делаешь?",
    "О чем хочешь поговорить?",
]


@app.route("/model", methods=["POST"])
def respond():
    """A handler of requests.
    To use:
    curl -X POST "http://localhost:${PORT}/model" \
    -H "accept: application/json"  -H "Content-Type: application/json" \
    -d "{ \"args\": [ \"data\" ]}"
    """
    st_time = time.time()
    # logger.info(f"input data: {request.json}")
    batch_len = len(request.json["x"])
    response = [
        [f"{random.choice(greeting_begin_texts)}\n{greeting_body_text}\n{random.choice(greeting_end_texts)}", 1.0]
        for _ in range(batch_len)
    ]
    total_time = time.time() - st_time
    logger.info(f"{SERVICE_NAME} exec time: {total_time:.3f}s")
    return jsonify(response)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=SERVICE_PORT)
