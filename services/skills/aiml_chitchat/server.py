# Copyright 2017 Neural Networks and Deep Learning lab, MIPT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import time
import traceback
import uuid

from flask import Flask, jsonify, request

from utils.programy_extention import MindfulDataFileBot
from utils.udpipe_preprocessing import get_preprocessor

# configuration
SERVICE_NAME = os.getenv("SERVICE_NAME", "unknow_skill")
SERVICE_PORT = int(os.getenv("SERVICE_PORT", 3000))
UDPIPE_MODEL_FILE = os.getenv("UDPIPE_MODEL_FILE", "")

preprocessor = get_preprocessor(UDPIPE_MODEL_FILE)

# trusted model
trusted_files = {
    "aiml": ["storage/trusted_categories"],
    "sets": ["storage/sets"],
}
trusted_chitchat_model = MindfulDataFileBot(trusted_files, "logging.yaml")

# untrusted model
untrusted_files = {
    "aiml": ["storage/untrusted_categories"],
    "sets": ["storage/sets"],
}
untrusted_chitchat_model = MindfulDataFileBot(untrusted_files, "logging.yaml")

models = [(trusted_chitchat_model, 1.0), (untrusted_chitchat_model, 0.7)]

app = Flask(__name__)


def infer(history):
    userid = uuid.uuid4().hex
    for uttr in history[::-1][::2][::-1]:
        words, _ = preprocessor(uttr)
        clean_text = " ".join(words)
        # print(uttr)
        # print(clean_text)
        responses = [(model.ask_question(userid, clean_text), conf) for model, conf in models]
        responses = [(text, conf) for text, conf in responses if text]
        responses = list(zip(*responses))
    if responses:
        return responses
    else:
        return ["Не знаю, что сказать", 0]


@app.route("/model", methods=["POST"])
def respond():
    """A handler of requests.
    To use:
    curl -X POST "http://localhost:${PORT}/model" \
    -H "accept: application/json"  -H "Content-Type: application/json" \
    -d "{ \"args\": [ \"data\" ]}"
    """
    st_time = time.time()

    history_batch = request.json.get("x", [])
    if not history_batch:
        print(f"input data: {request.json}")
    try:
        responses = [infer(history) for history in history_batch]
    except Exception:
        print(traceback.format_exc())
        responses = [["", 0]] * len(history_batch)
    total_time = time.time() - st_time
    print(f"{SERVICE_NAME} exec time: {total_time:.3f}s")
    print(f"out data: {responses}")
    return jsonify(responses)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=SERVICE_PORT)
