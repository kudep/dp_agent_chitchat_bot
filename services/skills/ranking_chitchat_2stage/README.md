## Description

This service is based on a DeepPavlov library 2 staged ranking chitchat model. The first stage is used tf-idf. The second stage is used, the BERT-based model. BERT was trained on [mltrack dataset](https://contest.yandex.ru/algorithm2018/contest/7914/problems/) released by Yandex.

Dataset of candidates was compiled by DeepPavlov from Web and include Dialogs of Yandex Alice.

## Configuration

There are placed `*.json` files, that provide parameters and data for models.
