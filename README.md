## chitchat_bot

This project provides modular chatbot based on [DeepPavlov Agent](https://github.com/deepmipt/dp-agent) and several services based on [DeepPavlov library](https://github.com/deepmipt/DeepPavlov). The chatbot is developed for support conversation in small talk style.

## Main description

This chatbot is based on DeepPavlov Agent. The agent performs services orchestration. An agent configuration is placed in `agent_configs`. `docker-compose.yml` file consists main docker-compose setup. `empty_*.yml` files consist especial docker-compose setup.

All services are placed in `services/${type_of_service}/${name_of_service}` directory with supporting documentation (`README.md`) files There are three service types:

- annotators - services, which make the preparation of messages from DeepPavlov Agent data flow and store preparation results in especial attributes of the message.
- selectors - services, which choose executable services (annotators, skills) for the next step of dataflow and which choose the best answer from a set of candidates.
- skills - services, which produce an answer candidate.
- deeppavlov - it's a directory that provides source code for other services (selectors, annotators, skills), which are based on DeepPavlov library.

Attention: All services are based on DeepPavlov library were trained by DeepPavlov methodology, you can check that in [DeepPavlov documentation](http://docs.deeppavlov.ai/en/master/).

## Requirements

- docker
- docker-compose

## QuickStart

### Run agent

Run chitchat bot with a full equipment mode.

```
docker-compose up -d
```

or run chatbot with a light mode without hardware resourceful services.

```
docker-compose -f docker-compose.yml -f empty_chitchat.yml -f empty_odqa.yml up -d --build
```

`DO NOT FORGET TO REBUILD CONTAINERS AFTER CHANGING THE CONFIGURATION FROM FULL MODE TO LIGHT MODE AND BACK`

or you can run chatbot with a proxy if it needs for agent connecting to the telegram.

```
docker-compose -f docker-compose.yml -f tg_proxy.yml up -d
```

### Start a conversation through the terminal

After the agent is started you can start a conversation through the terminal

```
docker-compose exec agent python -m deeppavlov_agent.run -pl dp-agent/pipeline_conf.yml -db dp-agent/db_conf.json
```

After that, you can enter a custom (any) user_id and start the dialogue.

### Start a conversation through the http-api-server

After the agent is started you can start a conversation through the http-api-server.
You can get more information about that from the [documentation](https://deeppavlov-agent.readthedocs.io/en/latest/intro/overview.html#http-api-server) of DeepPavlov Agent.

For testing, you can check agent by curl:

```
curl --header "Content-Type: application/json" \
     --request POST \
     --data '{"user_id":"xyz","payload":"hello"}' \
     http://localhost:4242

```

### Start a conversation through the telegram bot

For testing, you can only chitchat services without odqa and run local proxy:

```
docker-compose -f docker-compose.yml -f empty_odqa.yml -f tg_proxy.yml up -d
```

After that, run next command with your `TELEGRAM_TOKEN`:

```
docker-compose exec agent python dp-agent/run_telegram.py  -pl dp-agent/pipeline_conf.yml -db dp-agent/db_conf.json -t ${TELEGRAM_TOKEN} -px socks5://tg_proxy:9150
```

## Contributing

Contributing information is placed in `CONTRIBUTING.md` file.

## License

This project is licensed under the terms of the Apache 2.0 license.